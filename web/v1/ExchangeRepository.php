<?php
declare(strict_types=1);

namespace web\v1;

use Demodeos\Api\Core;
use Demodeos\BitrixExchange\Exchange;
use Demodeos\Contract\DTO\Exchange\RequestDTO;
use Demodeos\Contract\Models\Exchange\products_joined;
use Demodeos\DB\Connection;
use Demodeos\DB\QueryBuilder\QueryBuilder;
use stdClass;

class ExchangeRepository
{
    private static ExchangeRepository $_instance;
    private Connection $_sql;
    private string $products = 'products';
    private string $images = 'images';
    private string $attributes = 'attributes';
    private string $properties = 'properties';


    public function __construct()
    {
        ini_set("max_execution_time", "270");




    }



    public static function init(): static
    {
        self::$_instance ??= new ExchangeRepository();
        self::$_instance->_sql = new Connection(Core::$app->config('sql'));



        return self::$_instance;

    }

    public function parse()
    {

        $start = time();
        if(Core::$app->request()->get('clear') == 'true')
            Core::$app->sessions()->delete('files');

        if(!Core::$app->sessions()->get('files'))
        {
            $files = scandir(EXCHANGE_FILES);
            $files = array_filter($files, fn($el)=>$el!='.' && $el!='..');
            $files = array_map(fn($el)=>EXCHANGE_FILES.$el, $files);
            Core::$app->sessions()->set('files', $files);
        }
        else
        {
            $files = Core::$app->sessions()->get('files');
        }

        foreach ($files as $key=>$file)
        {


            $result = Exchange::init()->fromFile($file);

            self::$_instance->saveResult($result);

            unset($files[$key]);
            Core::$app->sessions()->set('files', $files);
            unlink($file);
            if((time() - $start) > 20) {
                header("Refresh: 10");
                MOX($files);
            }
        }




        return $result;

    }

    public function saveResult($result)
    {

        $result = json_decode(json_encode($result), true);


        foreach ($result as $key=>$data)
        {
           if(!empty($data))
           {
               foreach (array_chunk($data, 3000) as $values)
               {


                   $sql = QueryBuilder::insert($values)->table($key)->onDuplicateKey()->create();
                   $query = $this->_sql->query($sql->sql, $sql->params)->rows();

               }
           }


        }




    }

    public function getData()
    {

        $json = Core::$app->request()->body();
        $data = json_decode($json);
        $dto = (new RequestDTO())->load($data);

        $table = $dto->type;
        $limit = $dto->limit;
        $offset = $dto->offset;
        $params = $dto->params;

        if($limit)
            $limit = ' LIMIT '.$limit;
        else
            $limit = '';

        if($offset)
            $offset = ' OFFSET '.$offset;
        else
            $offset = '';


    $namespace = 'Demodeos\Contract\Models\Exchange\\';
    $class = $namespace.$table;




$SQL_ROWS = <<<ROWS
SELECT COUNT(*)
FROM $table

ROWS;

//$rows = $this->_sql->query($SQL_ROWS)->statement()->fetchColumn();

    $SQL = <<<SQL
    SELECT * FROM $table
    $limit
    $offset
    SQL;


    $result =$this->_sql->query($SQL)->fetchAll((new $class())::class);

    return $result;

    }

}