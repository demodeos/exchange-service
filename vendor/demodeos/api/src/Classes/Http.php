<?php
declare(strict_types=1);

namespace Demodeos\Api\Classes;

use Demodeos\Api\Core;
use Demodeos\Http\Cookies;
use Demodeos\Http\Headers;
use Demodeos\Http\Request;
use GuzzleHttp\Client;
use GuzzleHttp\Cookie\CookieJar;


class Http
{
    private static ?Http $_instance = null;
    private $_clients = [];

    private $current_service = null;

    private Request|null $request = null;


    private function __construct()
    {
        $this->request ??= new Request();
    }


    public static function service(string $service): static|bool
    {
        if(isset(Core::$app->config('services')[$service]))
        {
            self::$_instance ??= new self;
            self::$_instance->current_service = $service;

            $cookies = new CookieJar();
            $cookies = $cookies::fromArray($_COOKIE, 'devcomp.ru');
            $c = (new Headers())->get('cookie');

            $base_uri = Core::$app->config('services')[$service];
            self::$_instance->_clients[$service] ??= new Client([
                'base_uri'=>$base_uri,
                'verify'  => false,                        // если сайт использует SSL, откючаем для предотвращения ошибок
                'allow_redirects' => false,            // запрещаем редиректы
                'headers' => [                         // устанавливаем различные заголовки
                    'User-Agent'   => 'Mozilla/5.0 (Linux 3.4; rv:64.0) Gecko/20100101 Firefox/15.0',
                    'Accept'       => 'text/html,application/json,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
                    'Content-Type' => 'application/x-www-form-urlencoded',
                    'Cookie'=>$c
                        ],
                'cookies'=>true
            ]);
            /**
             * @var $c Client
             */
           $c =  self::$_instance->_clients[$service];

            return self::$_instance;
        }
        else
        {
            return false;
        }

    }


    public function get(string $endpoint)
    {
        /**
         * @var $client Client
         */
        $client = $this->_clients[$this->current_service];

        $result = $client->get($endpoint);

        $return = $result->getBody()->getContents();

        $c = new Cookies();

        $cookies = $client->getConfig('cookies')->toArray();
        foreach ($cookies as $cook)
        {
            $c->set($cook['Name'], $cook['Value']);
        }

        return json_decode($return);



    }

    public function put(string $endpoint)
    {
        /**
         * @var $client Client
         */
        $client = $this->_clients[$this->current_service];

        $result = $client->put($endpoint, [
            'form_params'=>$this->request->put()
        ]);

        $return = $result->getBody()->getContents();

        $c = new Cookies();

       $cookies = $client->getConfig('cookies')->toArray();
       foreach ($cookies as $cook)
       {
           $c->set($cook['Name'], $cook['Value']);
       }

        return json_decode($return);

    }

    public function post(string $endpoint)
    {
        /**
         * @var $client Client
         */
        $client = $this->_clients[$this->current_service];

        $result = $client->post($endpoint, [
            'form_params'=>$this->request->post()
        ]);

        $return = $result->getBody()->getContents();

        $c = new Cookies();

        $cookies = $client->getConfig('cookies')->toArray();
        foreach ($cookies as $cook)
        {
            $c->set($cook['Name'], $cook['Value']);
        }

        return json_decode($return);

    }

    public function delete(string $endpoint)
    {
        /**
         * @var $client Client
         */
        $client = $this->_clients[$this->current_service];

        $result = $client->delete($endpoint);

        $return = $result->getBody()->getContents();

        $c = new Cookies();

        $cookies = $client->getConfig('cookies')->toArray();
        foreach ($cookies as $cook)
        {
            $c->set($cook['Name'], $cook['Value']);
        }

        return json_decode($return);

    }

    public function setCookies($cookies)
    {


    }


}