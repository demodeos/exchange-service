<?php
declare(strict_types=1);

namespace Demodeos\Helpers;

use function mt_rand;
use function sprintf;
use function intval;
use function bin2hex;
use function function_exists;
use function random_bytes;
use function mcrypt_create_iv;
use function openssl_random_pseudo_bytes;
use function rand;


class Generator
{
    public static function UUIDv4() {
        return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
            mt_rand(0, 0xffff), mt_rand(0, 0xffff),
            mt_rand(0, 0xffff),
            mt_rand(0, 0x0fff) | 0x4000,
            mt_rand(0, 0x3fff) | 0x8000,
            mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
        );
    }
    public static function token($length = 32){
        if(!isset($length) || intval($length) <= 8 ){
            $length = 32;
        }
        if (function_exists('random_bytes')) {
            return bin2hex(random_bytes($length));
        }
        if (function_exists('mcrypt_create_iv')) {
            return bin2hex(mcrypt_create_iv($length, MCRYPT_DEV_URANDOM));
        }
        if (function_exists('openssl_random_pseudo_bytes')) {
            return bin2hex(openssl_random_pseudo_bytes($length));
        }
    }
    public static function password(int $max=6)
    {
        if($max<6)
            $max = 6;
        $chars="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
        $size=StrLen($chars)-1;
        $password=null;
        while($max--)
            $password.=$chars[rand(0,$size)];
        return $password;
    }

}