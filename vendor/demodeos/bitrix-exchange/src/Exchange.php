<?php
declare(strict_types=1);

namespace Demodeos\BitrixExchange;


use Demodeos\BitrixExchange\Parsers\ParserClassificator;
use Demodeos\BitrixExchange\Parsers\ParserContragents;
use Demodeos\BitrixExchange\Parsers\ParserManagers;
use Demodeos\BitrixExchange\Parsers\ParserPrices;
use Demodeos\BitrixExchange\Parsers\ParserPriceTypes;
use Demodeos\BitrixExchange\Parsers\ParserProducts;
use Demodeos\BitrixExchange\Parsers\ParserPropsValues;
use Demodeos\Contract\DTO\Exchange\ExchangeDTO;
use Demodeos\Contract\DTO\Exchange\ProductsExchangeDTO;
use XMLReader;
use DOMDocument;




class Exchange
{
const   PRICE           = 'prices',
        PRICE_TYPES     = 'price_types',
        PRODUCTS        = 'products',
        RESTS           = 'rests',
        CONTRAGENTS     = 'contragents',
        CONTRAGENTS_C   = 'contragents_customers',
        MANAGERS        = 'managers',
        USERS           = 'users',
        SALE            = 'sale',
        STOCK_TYPES     = 'stock_types',
        STOCK           = 'stock',
        PROPS_VALUES    = 'props_values',
        PROPS_OFFERS    = 'props_offers',
        CLASSIFICATOR   = 'classificator';


    private static ?Exchange $_instance = null;
    private string $_file;
    private string $schema_file_dir = __DIR__.DIRECTORY_SEPARATOR.'schema'.DIRECTORY_SEPARATOR;


    public static function init(): static
    {
        self::$_instance ??=new static();
        return self::$_instance;
    }






    public function fromFile($file = null)
    {
        $this->_file = $file;
        $reader = new XMLReader();
        if (!$reader->open($this->_file))
            die("Не удалось открыть файл {$this->_file}");

        $result = $this->detectXmlType();

        $parser = match ($result){
            self::PRODUCTS          =>  ParserProducts::parse($this->_file),
            self::CLASSIFICATOR     =>  ParserClassificator::parse($this->_file),
            self::PRICE             =>  ParserPrices::parse($this->_file),
            self::MANAGERS          =>  ParserManagers::parse($this->_file),
            self::PRICE_TYPES       =>  ParserPriceTypes::parse($this->_file),
            self::CONTRAGENTS       =>  ParserContragents::parse($this->_file),
            self::PROPS_VALUES      =>  ParserPropsValues::parse($this->_file),

            default => new ExchangeDTO()

        };

        return $parser;
    }
    protected function getIterator(string $file, string $tag, int $depth=null)
    {
        $reader = new XMLReader();
        if (!$reader->open($file))
            die("Failed to open {$file}");
        $doc = new DOMDocument;
        while($reader->read())
        {
            if($depth)
            {
                if ($reader->name === $tag && $reader->depth === 3)
                    $node = simplexml_import_dom($doc->importNode($reader->expand(), true));
                if(isset($node) && !empty($node))
                    yield $node;
                unset($node);
            }
            else
            {
                if ($reader->name === $tag)
                    $node = simplexml_import_dom($doc->importNode($reader->expand(), true));
                if(isset($node) && !empty($node))
                    yield $node;
                unset($node);
            }



        }

        $reader->close();
        return false;
    }

    public function detectXmlType(): string|bool
    {


        if($this->validateXml(self::PRODUCTS))
        {
            return self::PRODUCTS;
        }

        elseif ($this->validateXml(self::PRICE))
        {
            return self::PRICE;
        }
        elseif ($this->validateXml(self::RESTS))
        {
            return self::RESTS;
        }
        elseif ($this->validateXml(self::CLASSIFICATOR))
        {
            return self::CLASSIFICATOR;
        }

        elseif ($this->validateXml(self::CONTRAGENTS))
        {
            return self::CONTRAGENTS;
        }

        elseif ($this->validateXml(self::PRICE_TYPES))
        {
            return self::PRICE_TYPES;
        }

        elseif ($this->validateXml(self::MANAGERS))
        {
            return self::MANAGERS;
        }
        elseif ($this->validateXml(self::MANAGERS))
        {
            return self::MANAGERS;
        }

        elseif ($this->validateXml(self::STOCK_TYPES))
        {
            return self::STOCK_TYPES;
        }
        elseif ($this->validateXml(self::PROPS_VALUES))
        {
            return self::PROPS_VALUES;
        }
        elseif ($this->validateXml(self::PROPS_OFFERS))
        {
            return self::PROPS_OFFERS;
        }
        elseif ($this->validateXml(self::CONTRAGENTS_C))
        {
            return self::CONTRAGENTS_C;
        }




        return false;

    }

    private function validateXml(string $schema_filename): bool
    {
        $valid = true;
        $reader = new XMLReader();



        if (!$reader->open($this->_file))
            return false;

        $reader->setSchema($this->schema_file_dir.$schema_filename.'.xsd');



        $line = 0;
        while(@$reader->read())
        {
            if (!$reader->isValid()) {
                $valid = false;
                break;
            }
            if($schema_filename === self::CONTRAGENTS && $line>100)
                break;
            if($schema_filename === self::PRODUCTS && $line>100)
                break;

            $line++;
        }

        return $valid;
    }


}