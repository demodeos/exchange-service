<?php
declare(strict_types=1);

namespace Demodeos\BitrixExchange\Parsers;

use XMLReader;
use DOMDocument;


class AbstractParser
{
    protected static $_instance;
    protected string $file;

    public static function parse(string $file)
    {

        self::$_instance = new static();

        self::$_instance->file = $file;

        return self::$_instance->run();
    }

    protected function getIterator(string $tag, int $depth=null)
    {
        $reader = new XMLReader();
        if (!$reader->open($this->file))
            die("Failed to open {$this->file}");
        $doc = new DOMDocument;
        while($reader->read())
        {
            if($depth)
            {
                if ($reader->name === $tag && $reader->depth === 3)
                    $node = simplexml_import_dom($doc->importNode($reader->expand(), true));
                if(isset($node) && !empty($node))
                    yield $node;
                unset($node);
            }
            else
            {
                if ($reader->name === $tag)
                    $node = simplexml_import_dom($doc->importNode($reader->expand(), true));
                if(isset($node) && !empty($node))
                    yield $node;
                unset($node);
            }



        }

        $reader->close();
        return false;
    }


    public function run()
    {
        MOX(get_called_class());

    }

}