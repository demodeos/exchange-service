<?php
declare(strict_types=1);

namespace Demodeos\BitrixExchange\Parsers;


use Demodeos\Contract\DTO\Exchange\ExchangeDTO;

use Demodeos\Contract\Models\Exchange\attributes;
use Demodeos\Contract\Models\Exchange\images;
use Demodeos\Contract\Models\Exchange\products;
use Demodeos\Contract\Models\Exchange\properties;
use Demodeos\Helpers\Language;

class ParserProducts extends AbstractParser
{



        public function run(): ExchangeDTO
        {



            $DTO = new ExchangeDTO();

           $iterator = $this->getIterator('Товар');

            foreach ($iterator as $node)
           {


               $product = new products();

               $product->guid = (string)$node->Ид;
               $product->code = (string)$node->Штрихкод;
               $product->shortcode = preg_replace("/(^УТ-[0]{0,5})/", '', (string)$node->Штрихкод) ;
               $product->article = (string)$node->Артикул;
               $product->name = strip_tags ((string)$node->Наименование);
               $product->cef_name = Language::Translit($product->name);
               $product->category = (string)$node->Группы->Ид;
               $product->description = strip_tags ((string)$node->Описание);
               $product->manufacturer = (string)$node->Изготовитель->ОфициальноеНаименование;
               $product->nds = (float)$node->СтавкиНалогов->СтавкаНалога->Ставка;
               $product->visible = 1;

               foreach ($node->ЗначенияСвойств->ЗначенияСвойства as $attribute )
               {
                   $product_attribute = new attributes();
                   $product_attribute->product = $product->guid;
                   $product_attribute->attribute = (string)$attribute->Ид;
                   $product_attribute->value = (string)$attribute->Значение;
                   /**
                    * Запись модели в массив свойств продукта
                    */
                   $DTO->attributes[] = $product_attribute;
               }


               foreach ($node->ЗначенияРеквизитов->ЗначениеРеквизита as $property )
               {

                   $product_property = new properties();
                   $product_property->product = $product->guid;
                   $product_property->property = (string)$property->Наименование;
                   $product_property->value = (string)$property->Значение;
                   /**
                    * Запись модели в массив Реквизитов продукта
                    */
                   if($product_property->value!= '')
                   $DTO->properties[] = $product_property;
               }

               if(isset($node->Картинка))
                   foreach ((array)$node->Картинка as $image )
                   {

                       $images = new images();
                       $images->product = $product->guid;
                       $images->file = ''.$image;
                       $images->filename = '12312321';
                       $images->url = '1';
                       $images->visible = 1;
                       /**
                        * Запись модели в массив изображений продукта
                        */
                       $DTO->images[] = $images;
                   }

               $DTO->products[] = $product;
           }

            return $DTO;
        }




}

