<?php
declare(strict_types=1);

namespace Demodeos\BitrixExchange\Parsers;

use Demodeos\Contract\DTO\Exchange\ExchangeDTO;
use Demodeos\Contract\Models\Exchange\price_types;

class ParserPriceTypes extends AbstractParser
{



    public function run(): ExchangeDTO
    {

        $iterator = $this->getIterator('ЭлементСправочника');
        $DTO = new ExchangeDTO();

        foreach ($iterator as $data)
        {

            $price_types = new price_types();

            $data = json_decode(json_encode($data));

            $price_types->guid = (is_string($data->Ид))?$data->Ид:'';
            foreach ($data->ЗначенияРеквизитов->ЗначениеРеквизита as $value)
            {

                switch ($value->Наименование)
                {
                    case 'Наименование':
                        $price_types->name = (is_string($value->Значение))?$value->Значение:'';
                        break;
                    case 'Контрагент':
                        $price_types->contragent = (is_string($value->Значение))?$value->Значение:'';
                        break;
                    case 'Партнер':
                        $price_types->partner = (is_string($value->Значение))?$value->Значение:'';
                        break;
                    case 'ЦенаВключаетНДС':
                        $price_types->nds = ($value->Значение == 'false' || $value->Значение == null)? 0 : 1;
                        break;
                    case 'Статус':
                        $price_types->status = (is_string($value->Значение))?$value->Значение:'';
                        break;
                    case 'Менеджер':
                        $price_types->manager = (is_string($value->Значение))?$value->Значение:'';
                        break;
                    case 'W_НаценкаНаВидЦен':
                        $price_types->margin = (is_int($value->Значение) || is_string($value->Значение) || is_float($value->Значение))?$value->Значение:0;
                        break;
                    case 'W_ОбщаяСкидкаПоСоглашению':
                        $price_types->discount = (is_int($value->Значение) || is_string($value->Значение) || is_float($value->Значение))?$value->Значение:0;
                        break;
                    case 'ПометкаУдаления':
                        $price_types->deleted = ($value->Значение == 'false' || is_null($value->Значение))?0:1;
                        break;
                    case 'ДоступноВнешнимПользователям':
                        $price_types->active = ($value->Значение == 'false' || $value->Значение == null)?0:1;
                        break;
                    case 'Дата':
                        $price_types->date = (is_string($value->Значение))?$value->Значение:'';
                        break;
                }
            }
            if($price_types->deleted == 0)
                $DTO->price_types[] = $price_types;
            unset($price_types);



        }

    return $DTO;
     }
}