<?php
declare(strict_types=1);

namespace Demodeos\BitrixExchange\Parsers;

use Demodeos\Contract\DTO\Exchange\ExchangeDTO;
use Demodeos\Contract\Models\Exchange\prices;

class ParserPrices extends AbstractParser
{





    public function run(): ExchangeDTO
    {

        $iterator = $this->getIterator('Предложение');

        $DTO = new ExchangeDTO();

        foreach ($iterator as $data)
        {
            $product = (string)$data->Ид;
            foreach ($data->Цены->Цена as $prices)
            {
                $price = new prices();
                $price->guid = (string)$prices->ИдТипаЦены;
                $price->product = $product;
                $price->price = (string)$prices->ЦенаЗаЕдиницу;
                $price->nds = $prices->Налог->УчтеноВСумме == 'true'?1:0;
                $price->view = (string)$prices->Представление;
                $price->view = substr($price->view, strpos($price->view, 'RUB')+4);
                $DTO->prices[] = $price;
            }
        }


        return $DTO;
    }
}