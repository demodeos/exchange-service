<?php
declare(strict_types=1);

namespace Demodeos\BitrixExchange\Parsers;

use Demodeos\Contract\DTO\Exchange\ExchangeDTO;
use Demodeos\Contract\Models\Exchange\managers;

class ParserManagers extends AbstractParser
{

    public function run(): ExchangeDTO
    {
        $iterator = $this->getIterator('ЭлементСправочника');

        $DTO = new ExchangeDTO();

        foreach ($iterator as $value)
        {
            $data = new managers();
            $data->guid = (string)$value->Ид;

            foreach ($value->ЗначенияРеквизитов->ЗначениеРеквизита as $property)
            {
                if($property->Наименование == 'Наименование')
                    $data->name = (string)$property->Значение;
                if($property->Наименование == 'Подразделение')
                    $data->subdivision = (string)$property->Значение;
                if($property->Наименование == 'Email')
                    $data->email = (string)$property->Значение;
                if($property->Наименование == 'Недействителен')
                    $data->deleted = (string)$property->Значение == 'false'?0:1;
                if($property->Наименование == 'Наименование')
                    $data->name = (string)$property->Значение;


            }

            $DTO->managers[] = $data;
        }

        return $DTO;
       // MOX($DTO);
    }


}