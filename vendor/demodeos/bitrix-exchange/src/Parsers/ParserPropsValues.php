<?php
declare(strict_types=1);

namespace Demodeos\BitrixExchange\Parsers;

use Demodeos\Contract\DTO\Exchange\ExchangeDTO;
use Demodeos\Contract\Models\Exchange\props_values;
use Exception;

class ParserPropsValues extends AbstractParser
{

    public function run()
    {

        $iterator = $this->getIterator('Свойство');

        $DTO = new ExchangeDTO();

        foreach ($iterator as $data)
        {
            $data = json_decode(json_encode($data));

            if(isset($data->ВариантыЗначений) && !is_array($data->ВариантыЗначений->Справочник))
            {
                $temp = $data->ВариантыЗначений->Справочник;
                $props_values = new props_values();
                $props_values->guid = $data->Ид;
                $props_values->name = $data->Наименование;
                $props_values->value_guid = $temp->ИдЗначения;
                $props_values->value_name = $temp->Значение;

                $DTO->props_values[] = $props_values;
                unset($props_values);
            }
            elseif (isset($data->ВариантыЗначений) && is_array($data->ВариантыЗначений->Справочник))
            {
                foreach ($data->ВариантыЗначений->Справочник as $property)
                {
                    $props_values = new props_values();
                    $props_values->guid = $data->Ид;
                    $props_values->name = $data->Наименование;
                    $props_values->value_guid = $property->ИдЗначения;
                    $props_values->value_name = $property->Значение;
                    $DTO->props_values[] = $props_values;
                    unset($props_values);
                }
            }
        }

        return $DTO;

    }


}