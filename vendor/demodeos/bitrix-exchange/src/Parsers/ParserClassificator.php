<?php
declare(strict_types=1);

namespace Demodeos\BitrixExchange\Parsers;

use Demodeos\Contract\DTO\Exchange\ExchangeDTO;
use Demodeos\Contract\Models\Exchange\categories;
use Demodeos\Helpers\Language;
use XMLReader;
use DOMDocument;
use RecursiveIteratorIterator;
use RecursiveArrayIterator;
use Traversable;
use Iterator;


use function simplexml_import_dom;

class ParserClassificator extends AbstractParser
{




    public function run(): ExchangeDTO
    {

        $reader = new XMLReader();
        $DTO = new ExchangeDTO();

        $reader->open($this->file);

        $cat = [];

        $iterator = $this->getIterator('Группа' ,1);
        foreach ($iterator as $node)
        {



            $node = json_decode(json_encode($node));


           // $cat = $this->test($node);

            $this->parseCategory($node, $DTO->categories);

         //   $this->readCategory($node, $cat);
         // $DTO->categories =array_merge($DTO->categories, $cat);



        }



        return $DTO;

    }


    public function parseCategory($node, &$arr)
    {


        $model = new categories();
        $model->guid = $node->Ид;
        $model->name = $node->Наименование;
        $model->parent = 0;
        $model->cef_name = Language::Translit($model->name);
        $model->depth = 1;
        if(!isset($arr[$model->guid]))
            $arr[$model->guid] = $model;
        unset($model);


        if(isset($node->Группы->Группа) && is_array($node->Группы->Группа))
        {
            foreach ($node->Группы->Группа as $cat2)
            {
                $model = new categories();
                $model->guid = $cat2->Ид;
                $model->name = $cat2->Наименование;
                $model->parent = $node->Ид;
                $model->cef_name = Language::Translit($model->name);
                $model->depth = 2;

                if(!isset($arr[$model->guid]))
                    $arr[$model->guid] = $model;
                unset($model);

                if(isset($cat2->Группы->Группа) && is_array($cat2->Группы->Группа))
                {
                    foreach ($cat2->Группы->Группа as $cat3)
                    {
                        $model = new categories();
                        $model->guid = $cat3->Ид;
                        $model->name = $cat3->Наименование;
                        $model->parent = $cat2->Ид;
                        $model->cef_name = Language::Translit($model->name);
                        $model->depth = 3;
                        if(!isset($arr[$model->guid]))
                            $arr[$model->guid] = $model;
                        unset($model);
                    }

                }


            }

        }


        return $arr;

    }

    public function readCategory($category, & $result, $parent = '0', $depth = 1)
    {


        $catalog = new categories();
        $catalog->guid = (string)$category->Ид;
        $catalog->name = (string)$category->Наименование;
        $catalog->cef_name = Language::Translit($catalog->name);
        $catalog->parent = $parent;
        //  $catalog->alias_name = '';
        $catalog->depth = $depth;
        $result[] = $catalog;



        if(isset($category->Группы))
        {

            foreach ($category->Группы->Группа as $items)
                $this->readCategory($items, $result, $catalog->guid,2);
        }

        else
        {
            $catalog->depth = 3;
        }




        unset($catalog, $depth);

        return $result;
    }


}