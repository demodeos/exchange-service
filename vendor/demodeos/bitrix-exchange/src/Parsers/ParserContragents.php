<?php
declare(strict_types=1);

namespace Demodeos\BitrixExchange\Parsers;

use Demodeos\Contract\DTO\Exchange\ExchangeDTO;
use Demodeos\Contract\Models\Exchange\contragents;

class ParserContragents extends AbstractParser
{

    public function run(): ExchangeDTO
    {
        $DTO = new ExchangeDTO();

        $iterator = $this->getIterator('Контрагент');

        foreach ($iterator as $data)
        {
            $model = new contragents();

            $data = json_decode(json_encode($data));
            $model->guid = $data->Ид;
            $model->name = $data->Наименование;
            if(isset($data->ОфициальноеНаименование))
                $model->official = $data->ОфициальноеНаименование;
            elseif (isset($data->ПолноеНаименование))
                $model->official = (is_string($data->ПолноеНаименование))?$data->ПолноеНаименование:$model->name;
            else
                $model->official = $model->name;
            $model->inn = (is_string($data->ИНН) || is_numeric($data->ИНН))?$data->ИНН:0;
            $model->kpp = (is_string($data->КПП) || is_numeric($data->КПП))?$data->КПП:0;
            $model->okpo = (is_string($data->КодПоОКПО) || is_numeric($data->КодПоОКПО))?$data->КодПоОКПО:0;
            $model->deleted = ($data->ПометкаУдаления == 'false')?0:1;

            if($model->deleted == 0)
                $DTO->contragents[] = $model;
            unset($model);
        }

       // MOX($DTO);
        return $DTO;
    }


}