<?php
declare(strict_types=1);

namespace Demodeos\Contract\Models\Exchange;

class managers
{
    public $guid;
    public $name;
    public $subdivision;
    public $email;
    public $deleted;
}