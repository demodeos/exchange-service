<?php
declare(strict_types=1);

namespace Demodeos\Contract\Models\Exchange;

class props_values
{
    public $guid;
    public $name;
    public $value_guid;
    public $value_name;
}
