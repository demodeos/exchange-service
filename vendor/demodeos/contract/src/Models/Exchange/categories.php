<?php
declare(strict_types=1);

namespace Demodeos\Contract\Models\Exchange;

class categories
{
    public $guid;
    public $name;
    public $cef_name;
    public $parent;
    public $depth;

}