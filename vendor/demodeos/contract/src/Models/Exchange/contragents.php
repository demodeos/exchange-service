<?php
declare(strict_types=1);

namespace Demodeos\Contract\Models\Exchange;

class contragents
{
    public $guid;
    public $name;
    public $official;
    public $inn;
    public $kpp;
    public $okpo;
    public $deleted;

}