<?php
declare(strict_types=1);

namespace Demodeos\Contract\DTO\User;

use Demodeos\Contract\AbstractDTO;

class UserLayerDTO extends AbstractDTO
{
    public bool $error = false;
    public mixed $body;



}