<?php
declare(strict_types=1);

namespace Demodeos\Contract\DTO\Exchange;

class ManagerDTO
{
    public array $managers = [];
}